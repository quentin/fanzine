@react.component
let make = () => {

  module Proto = {
    type t = HTTP | HTTPS
    let default = HTTPS

    let make = str => switch(str) {
      | "http" => HTTP
      | _ => default
    }
    let make2 = mayStr => switch(mayStr) {
      | Some(str) => make(str)
      | None => default
    }

    let toStr = p => switch(p) {
      | HTTP => "http"
      | HTTPS => "https"
    }
    let isTls = p => p == HTTPS
  }

  module UrlStyle = {
    type t = VHOST | PATH
    let default = PATH

    let make = str => switch(str) {
      | "vhost" => VHOST
      | _ => default
    }
    let make2 = mayStr => switch(mayStr) {
      | Some(str) => make(str)
      | None => default
    }

    let toStr = u => switch(u) {
      | PATH => "path"
      | VHOST => "vhost"
    }
    let isPathStyle = u => u == PATH
  }

  module LocalKV = {
    open Dom
    let get = (key) => Storage2.localStorage -> Storage2.getItem(key) 
    let set = (key, value) => Storage2.localStorage -> Storage2.setItem(key, value)
  }

  module Config = {
    open Belt

    type t = {
      protocol: Proto.t,
      url: UrlStyle.t,
      endpoint: string,
      region: string,
      keyId: string,
      secretKey: string,
    }

    let make = () => { 
      protocol: "conf_protocol" -> LocalKV.get -> Proto.make2,
      url: "conf_url" -> LocalKV.get -> UrlStyle.make2, 
      endpoint: "conf_endpoint" -> LocalKV.get -> Option.getWithDefault("garage.deuxfleurs.fr"), 
      region: "conf_region" -> LocalKV.get -> Option.getWithDefault("garage"), 
      keyId: "conf_keyid" -> LocalKV.get -> Option.getWithDefault(""), 
      secretKey: "conf_secretkey" -> LocalKV.get -> Option.getWithDefault(""),
    }

    let build = c => {
      {
        S3.endpoint: Some({ 
          protocol: c.protocol -> Proto.toStr, 
	        hostname: c.endpoint, 
	        path: "/" 
        }),
        credentials: Some({ 
	        accessKeyId: c.keyId, 
	        secretAccessKey: c.secretKey,
        }),
        tls: Some(c.protocol -> Proto.isTls),
        forcePathStyle: Some(c.url -> UrlStyle.isPathStyle),
        region: Some(c.region),
      }
    }
  }

  let (config, updateConfig) = React.useState(_ => Config.make());

  let login = _ => {
    Js.log(config -> Config.build)
  }

  let setProtocol = evt => {
    let value = ReactEvent.Form.target(evt)["value"] -> Proto.make
    updateConfig(c => {...c, protocol: value})
  }

  let setEndpoint = evt => {
    let value = (ReactEvent.Form.target(evt)["value"])
    updateConfig(c => {...c, endpoint: value })
  }

  let setRegion = evt => {
    let value = (ReactEvent.Form.target(evt)["value"])
    updateConfig(c => {...c, region: value })
  }

  let setUrlStyle = evt => {
    let value = ReactEvent.Form.target(evt)["value"] -> UrlStyle.make
    updateConfig(c => {...c, url: value })
  }

  let setKeyId = evt => {
    let value = (ReactEvent.Form.target(evt)["value"])
    updateConfig(c => {...c, keyId: value })
  }
    
  let setSecretKey = evt => {
    let value = (ReactEvent.Form.target(evt)["value"])
    updateConfig(c => {...c, secretKey: value })
  }

  <section className="menu">
    <div className="menu-title">{ "Configuration"->React.string }</div>
    <form>
      <fieldset>
        <legend> { `Réseau` -> React.string } </legend>
	      <div className="group">
          <select name="protocol" onChange={setProtocol} value={config.protocol -> Proto.toStr}>
            <option value="https">{ "https://"->React.string }</option>
            <option value="http">{ "http://"->React.string }</option>
          </select>
          <input name="endpoint" type_="text" placeholder="garage.example.tld" onChange={setEndpoint} value={config.endpoint} />
   	    </div>
	      <div className="group">
          <input name="region" type_="text" placeholder="garage" onChange={setRegion} value={config.region} />
	        <select name="urlStyle" onChange={setUrlStyle} value={config.url -> UrlStyle.toStr}>
	          <option value="path">{ "chemin"->React.string }</option>
	          <option value="vhost">{ `hôte`->React.string }</option>
	        </select>
	      </div>
      </fieldset>

      <fieldset>
        <legend> { "Authentification"->React.string }</legend>
	      <div className="group">
          <input name="keyId" type_="text" placeholder="GKxxx" onChange={setKeyId} value={config.keyId} />
	      </div>
	      <div className="group">
          <input name="secretKey" type_="password" placeholder="************" onChange={setSecretKey} value={config.secretKey} />
	      </div>
      </fieldset>

      <input className="primary" type_="button" onClick={login} value="Valider"/>
    </form>
  </section>
}
