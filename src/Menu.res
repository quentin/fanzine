@react.component
let make = () => {
  <section className="menu">
    <div className="menu-title">{ React.string("Fanzine")}</div>
    <a className="menu-elem" href="#plain-text">{ React.string("Texte brut")}</a>
    <div className="menu-elem">{ React.string("Galerie photo")}</div>
    <div className="menu-elem">{ React.string("Fichiers")}</div>
    <hr/>
    <div className="menu-elem">{ React.string("Site statique")}</div>
    <div className="menu-elem">{ React.string("Explorateur de fichier")}</div>
    <hr/>
    <div className="menu-elem">{ React.string(`Paramètres`)}</div>
  </section>
}
