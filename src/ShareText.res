@react.component
let make = () => {

  <section id="plain-text">
    <div className="menu-title"><a href="#home">{ React.string("<") }</a> { React.string("Texte brut") }</div>
    <textarea id="plain-text-content" placeholder=`Collez votre texte à partager ici`></textarea>
    <a className="primary" id="plain-text-send" href="#plain-text-done"> { React.string("Envoyer") }</a>
  </section>
}
