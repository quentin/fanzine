type appState = {
  client: option<S3.client>,
  config: option<S3.config>,
}

@scope("JSON") @val
external parseS3Config: string => S3.config = "parse"

let buildState = () =>  {
  let maybeConfig = Dom.Storage2.localStorage -> Dom.Storage2.getItem("s3.config.json")

  let config = switch (maybeConfig) {
  | Some(data) => Some(data -> parseS3Config)
  | None => None
  }

  let client = switch (config) {
  | Some(data) => Some(data -> S3.createClientWithConfig)
  | None => None
  }

  { client: client, config: config }
}

@react.component
let make = () => {
  let (state, setState) = React.useState(buildState);

  switch (state.client) {
  | None => <Settings/>
  | Some(_) => <div><Menu/><ShareText/></div>
  }
}
