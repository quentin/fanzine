type client

type endpoint = {
  protocol: string,
  hostname: string,
  path: string,
}

type credentials = {
  accessKeyId: string,
  secretAccessKey: string,
}

type config = {
  endpoint: option<endpoint>,
  credentials: option<credentials>,
  tls: option<bool>,
  forcePathStyle: option<bool>,
  region: option<string>,
}


@module("@aws-sdk/client-s3") @new external createClientWithConfig: config => client = "S3Client"

module ListBuckets = {
  @module("@aws-sdk/client-s3") @new external new: {..} => AwsSdkV3.S3.ListBuckets.t = "ListBucketsCommand"
  let make = () => Js.Obj.empty() -> new
  @send external send: (client, AwsSdkV3.S3.ListBuckets.t) => Js.Promise.t<AwsSdkV3.S3.ListBuckets.response> = "send"
}

/*
let s3 = createClientWithConfig({})

ListBuckets.send(s3, ListBuckets.make())
   -> Promise.then(result => {
     Js.log((result.owner -> Belt.Option.getExn).id)
     Promise.resolve()
   })
   -> Promise.catch(error => {
     Js.log(error)
     Promise.resolve()
   })
   -> ignore
*/
