import path from 'path';
import { fileURLToPath } from 'url';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

export default {
    entry: {
        main: "./lib/es6/src/Main.mjs",
    },
    resolve: {
      modules: [path.resolve(__dirname, 'node_modules'), 'node_modules']
    },
    mode: 'production',
    output: {
        filename: "fanzine.js",
        path: path.resolve(__dirname, "static/popup/"),
    }
};
