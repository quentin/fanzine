# Fanzine

## Compile Reason

```
npm install
npx rescript
npx webpack-cli
```

## Add the extension to Firefox

  1. Run `about:debugging`
  2. Click on "This Firefox"
  3. Click on "Load a temporary module"
  4. Select Fanzine's `manifest.json` file on your file system
  5. Click "Refresh" if your modification does not appear automatically

## Vim plugin

For vim > 8.x

```bash
git clone https://github.com/rescript-lang/vim-rescript ~/.vim/pack/all/start/vim-rescript
```

https://github.com/rescript-lang/vim-rescript
